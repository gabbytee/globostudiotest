﻿var MainController = function () {

    var context = this;

    this.init = function () {
        renderProgress('Preparando presentación. Obteniendo fechas...',0);
        getDates().then(function () {
            renderProgress('Preparando presentación. Obteniendo compañías...', 20);
            getCompanies().then(function () {
                renderProgress('Preparando presentación. Obteniendo totales locales...', 40);  
                renderCells('local').then(function () {
                    context.column = 0;
                    $('.celldate:nth-child(n+4), #mainGrid>table>tbody>tr>td:nth-child(n+6)').css('display', 'none');
                    $('#scrollleft').css('opacity', 0.25).attr('active','false');
                    if (context.dates.length > 3) {
                        $('#scrollright').css('opacity',1).attr('active','true');
                    }
                    else {
                        $('#scrollright').css('opacity', 0.25).attr('active','false');
                    }
                    renderProgress();
                });
            });
        });
        $('#sincronizar').click(function () {
            var codes = [];
            $('.chkcompany:checked').each(function (index, value) {
                var companyCode = $(this).attr('data-companycode');
                if ($('#' + companyCode).css('display') != 'none') {
                    codes.push(companyCode);
                }
            })
            if (codes.length > 0) {
                sincronizar(codes);
            }
            else {
                toastr.warning('Seleccione al menos una compañía para realizar la sincronización');
            }
        });
        $('#seltodos').click(function (event) {
            if (this.checked) {
                $('.chkcompany').each(function () { this.checked = true });
            }
            else {
                $('.chkcompany').each(function () { this.checked = false })
            }
        });
        $('#buscar').click(function () {
            if (context.progress == null) {
                var txtbusqueda = $('#txtbusqueda');
                if (txtbusqueda.css('display') == 'none') {
                    txtbusqueda.show(250);
                }
            }
        });
        $('#txtbusquedaInput').on('keyup', function () {
            if (context.progress == null) {
                buscar($('#txtbusquedaInput').val());
            }
        });
        $('#cancelbusqueda').click(function () {
            if (context.progress == null) {
                $('#txtbusquedaInput').val('');
                $('#txtbusqueda').hide(250);
                $.each(context.companies, function (key, val) {
                    $('#' + val.strPkCode).show(500);
                });
            }
        });
        $('#scrollleft').click(function () {
            if ($(this).attr('active') == 'true') {
                context.column--;
                $('.celldate:nth-child(' + (4 + context.column) + '), #mainGrid>table>tbody>tr>td:nth-child(' + (6 + context.column) + ')').hide();
                $('.celldate:nth-child(' + (1 + context.column) + '), #mainGrid>table>tbody>tr>td:nth-child(' + (3 + context.column) + ')').show();
                $('#scrollright').css('opacity', 1).attr('active', 'true');
                if (context.column > 0) {
                    $('#scrollleft').css('opacity', 1).attr('active', 'true');
                }
                else {
                    $('#scrollleft').css('opacity', 0.25).attr('active', 'false');
                }
            }
        });
        $('#scrollright').click(function () {
            if ($(this).attr('active') == 'true') {
                context.column++;
                $('.celldate:nth-child('+(context.column)+'), #mainGrid>table>tbody>tr>td:nth-child('+(2+context.column)+')').hide();
                $('.celldate:nth-child(' + (3 + context.column) + '), #mainGrid>table>tbody>tr>td:nth-child(' + (5 + context.column) + ')').show();
                $('#scrollleft').css('opacity', 1).attr('active', 'true');
                if (context.dates.length > 3 + context.column) {
                    $('#scrollright').css('opacity', 1).attr('active', 'true');
                }
                else {
                    $('#scrollright').css('opacity', 0.25).attr('active', 'false');
                }
            }
        });
    }
    
    var sincronizar = function (companyList) {
        context.companyList = companyList;
        renderProgress('Realizando petición de datos al servicio... (0/' + companyList.length + ')', 0);
        getData(companyList)
        .done(function (data) {
            saveOrders(data)
            .then(function (data) {
                getDates()
                .then(function (data) {
                    renderCells()
                    .then(function () {
                        toastr.info('Se actualizó la información');
                        setTimeout(function () {
                            $('.okicon').hide(1500);
                        }, 15000);
                        renderProgress();
                    });
                });
            });
        })
        .fail(function (result) {
            console.log(result);
            renderProgress();
            toastr.error('Se presentaron errores en la petición de datos al servicio.<br>Revise los detalles en la consola.')
        });
    };

    var getData = function (companyList) {
        var deferred = $.Deferred();
        var n = companyList.length;
        for (var i = 0; i < n; i++) {
            var company = companyList[i];
            var datos = [];
            getDataByCompany(company)
                .always(function (data) {
                    renderProgress('Realizando petición de datos al servicio... (' + i + '/' + n + ')', i * 25 / n);
                    datos.push({ companyCode: data.companyCode, success: data.success, orders: data.items, message: data.message });
                    if (datos.length == companyList.length) {
                        var result = $.grep(datos, function (elem, index) {
                            return elem.success == false;
                        });
                        if (result.length > 0) {
                            deferred.reject(result);
                        } else {
                            deferred.resolve(datos);
                        }
                        
                    }
                });
        }
        return deferred.promise();
    };

    var getDataByCompany = function (companyCode) {
        var deferred = $.Deferred();
        $.ajax({
            url: 'http://190.0.61.94/GSDataService/Service.svc/GetSalesData',
            type: 'POST',
            dataType: 'json',
            data: '{"parameters": {"SessionId":"64CD19D0-89CD-4F9C-8101-5118C5F0106D","CompanyCode":"' + companyCode + '"}}',
            contentType: 'application/json'
        }).done(function (data) {
            deferred.resolve({ success: true, companyCode: companyCode, items: JSON.parse(data.d) });
        }).fail(function (data) {
            console.log(data);
            deferred.reject({ success: false, companyCode: companyCode, items: [], message: data.responseText });
        });
        return deferred.promise();
    };

    var saveOrders = function (orders) {
        var deferred = $.Deferred();
        renderProgress('Almacenando órdenes localmente... (0/' + orders.length + ')', 25);
        context.ordersSavedByCompany = 0;
        for (var i = 0; i < orders.length; i++) {
            var company = orders[i].companyCode;
            var datosOrders = [];
            saveOrdersByCompany(company, orders)
                .then(function (data) {
                    context.ordersSavedByCompany++;
                    renderProgress('Almacenando órdenes localmente... (' + context.ordersSavedByCompany + '/' + orders.length + ')', 25 + context.ordersSavedByCompany * 50 / orders.length);
                    datosOrders.push(data);
                    if (datosOrders.length == orders.length) {
                        deferred.resolve(datosOrders)
                    }
                });
        }
        return deferred.promise();
    };

    var saveOrdersByCompany = function (companyCode, datos) {
        var deferred = $.Deferred();
        var ndatos = $.grep(datos, function (elem, index) {
            return elem.companyCode == companyCode;
        });
        $.ajax({
            url: 'Orders/SaveCollection',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(ndatos[0]),
            contentType: 'application/json'
        }).done(function (data) {
            deferred.resolve({ companyCode: companyCode, result: data });
        });
        return deferred.promise();
    };

    var getDates = function () {
        var deferred = $.Deferred();
        var progress = $('#progress');
        $.ajax({
            url: 'Orders/GetDates',
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {
            context.dates = data;
            renderDates(data);
            deferred.resolve(data);
        });
        return deferred.promise();
    };

    var getCompanies = function () {
        var deferred = $.Deferred();
        $.ajax({
            url: 'Orders/GetCompanies',
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {
            context.companies = data;
            renderCompanies(data);
            deferred.resolve(data);
        });
        return deferred.promise();
    };

    var renderCompanies = function (data) {
        if (data.length > 0) {
            var txtRow = '';
            var rowCompanies = {};
            for (var i = 0; i < data.length; i++) {
                rowCompanies[data[i].strPkCode] = '<td class="cellcompany"><input type="checkbox" data-companycode="' + data[i].strPkCode + '" class = "chkcompany"/></td><td>' + data[i].strName + '</td>';
                txtRow += '<tr id = "' + data[i].strPkCode + '">' + rowCompanies[data[i].strPkCode] + '</tr>';
            }
            context.rowCompanies = rowCompanies
            $('#mainGrid table tbody').html(txtRow);
        };
    };

    var renderDates = function (data) {
        if (data.length > 0) {
            var txtRow = '';
            for (var i = 0; i < data.length; i++) {
                txtRow += '<td class="celldate">' + data[i] + '</td>';
            }
        }
        $('#fechas').html(txtRow);
    };

    var renderCells = function (type) {
        var deferred = $.Deferred();
        var datos = [];
        var companies = (type == 'local' ? context.companies : context.companyList);
        for (var i = 0; i < companies.length; i++) {
            renderCellsByCompany(type == 'local' ? companies[i].strPkCode : companies[i], type).then(function (data) {
                datos.push(data);
                if (datos.length == companies.length) {
                    for (var j = 0; j < datos.length; j++) {
                        var row = datos[j];
                        for (var k = 0; k < row.length; k++) {
                            $('#tot_' + row[k].id).html(row[k].html).removeClass('cellcalculando').addClass('celltotal');
                        }
                    }
                    deferred.resolve();
                }
            });
        }
        return deferred.promise();
    };

    var renderCellsByCompany = function (companyCode, type) {
        var deferred = new $.Deferred();
        var datos = [];
        var txtAnt = $('#' + companyCode).html();
        var txtRender = '';
        for (var i = 0; i < context.dates.length; i++) {
            txtRender += '<td id="tot_' + companyCode + context.dates[i] + '" class="cellcalculando"><img src="/Resources/ajax-loader.gif" /> Calculando...</td>';
        }
        $('#' + companyCode).html(context.rowCompanies[companyCode] + txtRender);
        for (var j = 0; j < context.dates.length; j++) {
            var dateSold = context.dates[j];
            $.ajax({
                url: 'Orders/GetTotal',
                type: 'POST',
                dataType: 'json',
                data: {
                    companyCode: companyCode,
                    dateSold: dateSold
                }
            }).done(function (data) {
                datos.push({
                    id: companyCode + data.dateSold,
                    value: data.total,
                    html: '<div>' + (type != 'local' ? '<div class="okicon">&nbsp;</div>' : '') + '<div class="celltotal">$ ' + Number(data.total).format(0, 3, '.', ',') + '</div></div>'
                });
                if (datos.length == context.dates.length) {
                    deferred.resolve(datos);
                }
            });
        }
        return deferred.promise();
    }

    var buscar = function (txtBusqueda) {
        var companiesIn = $.grep(context.companies, function (elem, index) {
            return elem.strName.search(new RegExp(txtBusqueda, 'i')) == -1;
        }, true);
        var companiesOut = $.grep(context.companies, function (elem, index) {
            return elem.strName.search(new RegExp(txtBusqueda, 'i')) == -1;
        });
        $.each(companiesIn, function (key, val) {
            $('#' + val.strPkCode).show(500);
        });
        $.each(companiesOut, function (key, val) {
            $('#' + val.strPkCode + ' input[data-companyCode="' + val.strPkCode + '"]').attr('checked', false);
            $('#' + val.strPkCode).hide(500);
        });
    };

    var renderProgress = function (txt, progress) {
        $('#progress').html(txt == undefined && progress == undefined ? '' : txt)
            .css('display', txt == undefined && progress == undefined ? 'none' : 'block');
        $('#buscar').css('opacity', txt == undefined && progress == undefined ? 1 : 0.25);
        $('#sincronizartext').css('display', txt == undefined && progress == undefined ? 'block' : 'none');
        $('#progressbar').css('display', txt == undefined && progress == undefined ? 'none' : 'block');
        if (txt == undefined && progress == undefined) {
            context.progress = null;
        }
        else {
            progress = (isNaN(Number(progress)) || progress == undefined) ? 0 : Number(progress);
            context.progress = progress;
            $('#progressbar').css('width', progress + '%');
        }
    }
};

$(document).ready(function () {
    Number.prototype.format = function (n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
    var mainController = new MainController();
    mainController.init();
});