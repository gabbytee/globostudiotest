﻿using GloboStudio01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GloboStudio01.Controllers
{
    public class OrdersController : Controller
    {
        EntityModel db = new EntityModel();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetCompanies()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var data = db.doCompany.ToList();
            return Json(data);
        }

        [HttpPost]
        public JsonResult GetDates()
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                object data = db.doOrderDetail.Select(q => q.datDateSold.ToString()).Distinct().OrderBy(x => x);
                return Json(data);
            }
            catch (Exception exc)
            {
                return Json(new { error = exc.Message });
            }
        }

        [HttpPost]
        public JsonResult GetTotal(string companyCode, DateTime dateSold)   // Evaluar enviar un arregle de fechas y totales
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                int total = 0;
                var result = db.doOrderDetail.Where(q => q.strFkCompanyCode == companyCode && q.datDateSold == dateSold);
                if (result.Any())
                {
                    total = (int) Math.Round(result.Sum(p => p.decCostPerUnit * p.intQuantity));
                }
                return Json(new { dateSold = dateSold.ToString("yyyy-MM-dd"), total = total });
            }
            catch (Exception exc)
            {
                return Json(new { error = exc.Message });
            }
        }

        [HttpPost]
        public JsonResult Save(string companyCode, doOrderRaw order)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            var observaciones = "";
            try
            {
                // 1) Verificar si existe companyCode en tblCompany
                if (db.doCompany.FirstOrDefault(q => q.strPkCode == companyCode) == null)
                {
                    throw new Exception("No existe el código de empresa");
                }
                // 2) Mapear propiedades de objeto recibido a doOrderFromData
                var orderFromData = new doOrderFromData
                {
                    strCompanyCode = companyCode,
                    strCompanyCodeFromData = order.CompanyCode,
                    strOrderNumber = order.OrderNumber,
                    datDateSold = order.DateSold,
                    strCustomerCode = order.CustomerCode,
                    strCustomerName = order.CustomerName,
                    strCustomerAddress = order.CustomerAddress,
                    strItemDescription = order.ItemDescription,
                    intQuantity = order.Quantity,
                    decCostPerUnit = order.CostPerUnit
                };
                // 3) Validar si se puede agregar a la colección de doOrderFromData
                db.doOrderFromData.Add(orderFromData);
                // 4) Si no encuentra CustomerCode en tblCustomer, adicionarlo
                // 5) Si sí lo encuentra, verificar que los demás datos sean los mismos y en caso negativo, actualizarlos.
                var customer = db.doCustomer.FirstOrDefault(q => q.strPkCode == order.CustomerCode);
                if (customer == null)
                {
                    db.doCustomer.Add(new doCustomer
                    {
                        strPkCode = order.CustomerCode,
                        strName = order.CustomerName,
                        strAddress = order.CustomerAddress
                    });
                    observaciones += "El cliente " + order.CustomerCode + " no existía. Se agregó a la tabla respectiva. ";
                }
                else
                {
                    if (customer.strAddress != order.CustomerAddress)
                    {
                        customer.strAddress = order.CustomerAddress;
                        observaciones += "Se actualizó la dirección del cliente " + order.CustomerCode + " en la tabla respectiva. ";
                    }
                    if (customer.strName != order.CustomerName)
                    {
                        customer.strName = order.CustomerName;
                        observaciones += "Se actualizó el nombre del cliente " + order.CustomerCode + " en la tabla respectiva. ";
                    }
                }
                // 6) Si no encuentra ProductCode en tblProduct, crearlo.
                // 7) Si sí lo encuentra, verificar que el CostPerUnit sea el mismo, y en caso negativo, actualizarlo.
                var product = db.doProduct.FirstOrDefault(q => q.intPkCode == orderFromData.intProductCode);
                if (product == null)
                {
                    db.doProduct.Add(new doProduct
                    {
                        intPkCode = orderFromData.intProductCode,
                        decCostPerUnit = order.CostPerUnit
                    });
                    observaciones += "El producto " + orderFromData.intProductCode.ToString() + " no existía. Se agregó a la tabla respectiva. ";
                }
                else
                {
                    if (product.decCostPerUnit != order.CostPerUnit)
                    {
                        product.decCostPerUnit = order.CostPerUnit;
                        observaciones += "Se actualizó el valor unitario del producto " + orderFromData.intProductCode.ToString() + " en la tabla respectiva. ";
                    }
                }
                // 8) Adicionar a la colección de tblOrderDetail
                var orderDetail = new doOrderDetail
                {
                    strNumber = order.OrderNumber,
                    strFkCompanyCode = companyCode,
                    datDateSold = order.DateSold,
                    strFkCustomerCode = order.CustomerCode,
                    intFkProduct = orderFromData.intProductCode,
                    intQuantity = order.Quantity,
                    decCostPerUnit = order.CostPerUnit
                };
                db.doOrderDetail.Add(orderDetail);
                // 9) Si todo el proceso fue satisfactorio, retornar el tblOrderDetail
                db.SaveChanges();
                return Json(new
                {
                    success = true,
                    data = new
                    {
                        intId = orderDetail.intId,
                        strNumber = order.OrderNumber,
                        strFkCompanyCode = companyCode,
                        datDateSold = order.DateSold,
                        strFkCustomerCode = order.CustomerCode,
                        intFkProduct = orderFromData.intProductCode,
                        intQuantity = order.Quantity,
                        decCostPerUnit = order.CostPerUnit
                    },
                    message = "Sincronización OK. " + observaciones
                });

            }
            catch (Exception exc)
            {
                // 10) Si algo falla, reportar error y retornar el objeto order
                return Json(new { success = false, data = order, message = exc.Message });
            }
        }

        [HttpPost]
        public JsonResult SaveCollection(string companyCode, List<doOrderRaw> orders)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
            var observaciones = "";     // Para implementar un log de errores
            // 0) Verificar si existe companyCode en tblCompany
            if (db.doCompany.FirstOrDefault(q => q.strPkCode == companyCode) == null)
            {
                throw new Exception("No existe el código de empresa");
            }
            try
            {
                // 1) Limpiar registros
                var orderstoclean = db.doOrderDetail.Where(q => q.strFkCompanyCode == companyCode);
                foreach (var ordertoclean in orderstoclean)
                {
                    db.doOrderDetail.Remove(ordertoclean);
                }
                foreach (var order in orders)
                {
                    try
                    {
                        // 2) Mapear propiedades de objeto recibido a doOrderFromData
                        var orderFromData = new doOrderFromData
                        {
                            strCompanyCode = companyCode,
                            strCompanyCodeFromData = order.CompanyCode,
                            strOrderNumber = order.OrderNumber,
                            datDateSold = order.DateSold,
                            strCustomerCode = order.CustomerCode,
                            strCustomerName = order.CustomerName,
                            strCustomerAddress = order.CustomerAddress,
                            strItemDescription = order.ItemDescription,
                            intQuantity = order.Quantity,
                            decCostPerUnit = order.CostPerUnit
                        };
                        // 3) Validar si se puede agregar a la colección de doOrderFromData
                        db.doOrderFromData.Add(orderFromData);
                        // 4) Si no encuentra CustomerCode en tblCustomer, adicionarlo
                        // 5) Si sí lo encuentra, verificar que los demás datos sean los mismos y en caso negativo, actualizarlos.
                        var customer = db.doCustomer.Local.FirstOrDefault(q => q.strPkCode == order.CustomerCode);
                        var customers = db.doCustomer.Local.Where(q => q.strPkCode == order.CustomerCode).Count();
                        customers += db.doCustomer.Where(q => q.strPkCode == order.CustomerCode).Count();
                        if (customers == 0)
                        {
                            db.doCustomer.Add(new doCustomer
                            {
                                strPkCode = order.CustomerCode,
                                strName = order.CustomerName,
                                strAddress = order.CustomerAddress
                            });
                            observaciones += "El cliente " + order.CustomerCode + " no existía. Se agregó a la tabla respectiva.<br/>";
                        }
                        else
                        {
                            if (customer != null)
                            {
                                if (customer.strAddress != order.CustomerAddress)
                                {
                                    customer.strAddress = order.CustomerAddress;
                                    observaciones += "Se actualizó la dirección del cliente " + order.CustomerCode + " en la tabla respectiva.<br/>";
                                }
                                if (customer.strName != order.CustomerName)
                                {
                                    customer.strName = order.CustomerName;
                                    observaciones += "Se actualizó el nombre del cliente " + order.CustomerCode + " en la tabla respectiva.<br/>";
                                }
                            }
                        }
                        // 6) Si no encuentra ProductCode en tblProduct, crearlo.
                        // 7) Si sí lo encuentra, verificar que el CostPerUnit sea el mismo, y en caso negativo, actualizarlo.
                        var product = db.doProduct.Local.FirstOrDefault(q => q.intPkCode == orderFromData.intProductCode);
                        var products = db.doProduct.Local.Where(q => q.intPkCode == orderFromData.intProductCode).Count();
                        products += db.doProduct.Where(q => q.intPkCode == orderFromData.intProductCode).Count();
                        if (products == 0)
                        {
                            db.doProduct.Add(new doProduct
                            {
                                intPkCode = orderFromData.intProductCode,
                                decCostPerUnit = order.CostPerUnit
                            });
                            observaciones += "El producto " + orderFromData.intProductCode.ToString() + " no existía. Se agregó a la tabla respectiva.<br/>";
                        }
                        else
                        {
                            if (product != null)
                            {
                                if (product.decCostPerUnit != order.CostPerUnit)
                                {
                                    product.decCostPerUnit = order.CostPerUnit;
                                    observaciones += "Se actualizó el valor unitario del producto " + orderFromData.intProductCode.ToString() + " en la tabla respectiva.<br/>";
                                }
                            }
                        }
                        // 8) Adicionar a la colección de tblOrderDetail
                        var orderDetail = new doOrderDetail
                        {
                            strNumber = order.OrderNumber,
                            strFkCompanyCode = companyCode,
                            datDateSold = order.DateSold,
                            strFkCustomerCode = order.CustomerCode,
                            intFkProduct = orderFromData.intProductCode,
                            intQuantity = order.Quantity,
                            decCostPerUnit = order.CostPerUnit
                        };
                        db.doOrderDetail.Add(orderDetail);
                    }
                    catch (Exception exc)
                    {
                        // 10) Si algo falla, reportar error y retornar el objeto order
                        throw new Exception(exc.Message);
                    }

                }
                // 9) Si todo el proceso fue satisfactorio, persistir cambios y retornar tblOrderDetail
                db.SaveChanges();
                return Json(new
                {
                    success = true,
                    message = "Sincronización de empresa " + companyCode + " correcta.<br/>" + observaciones
                });
            }
            catch (Exception exc)
            {
                return Json(new { success = false, data = orders, message = exc.Message });
            }
        }

    }
}