namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblCustomer")]
    public partial class doCustomer
    {
        public doCustomer()
        {
            doOrderDetail = new HashSet<doOrderDetail>();
        }

        [Key]
        [StringLength(3)]
        public string strPkCode { get; set; }

        [Required]
        [StringLength(100)]
        public string strName { get; set; }

        [Required]
        [StringLength(100)]
        public string strAddress { get; set; }

        public virtual ICollection<doOrderDetail> doOrderDetail { get; set; }
    }
}
