namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblProduct")]
    public partial class doProduct
    {
        public doProduct()
        {
            doOrderDetail = new HashSet<doOrderDetail>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int intPkCode { get; set; }

        public decimal decCostPerUnit { get; set; }

        public virtual ICollection<doOrderDetail> doOrderDetail { get; set; }
    }
}
