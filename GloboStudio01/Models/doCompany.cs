namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblCompany")]
    public partial class doCompany
    {
        public doCompany()
        {
            doOrderDetail = new HashSet<doOrderDetail>();
        }

        [Key]
        [StringLength(3)]
        public string strPkCode { get; set; }

        [Required]
        [StringLength(100)]
        public string strName { get; set; }

        public virtual ICollection<doOrderDetail> doOrderDetail { get; set; }
    }
}
