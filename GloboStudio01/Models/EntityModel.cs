namespace GloboStudio01.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EntityModel : DbContext
    {
        public EntityModel()
            : base("name=EntityModel")
        {
        }

        public virtual DbSet<doCompany> doCompany { get; set; }
        public virtual DbSet<doCustomer> doCustomer { get; set; }
        public virtual DbSet<doOrderDetail> doOrderDetail { get; set; }
        public virtual DbSet<doOrderFromData> doOrderFromData { get; set; }
        public virtual DbSet<doProduct> doProduct { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<doCompany>()
                .Property(e => e.strPkCode)
                .IsUnicode(false);

            modelBuilder.Entity<doCompany>()
                .Property(e => e.strName)
                .IsUnicode(false);

            modelBuilder.Entity<doCompany>()
                .HasMany(e => e.doOrderDetail)
                .WithRequired(e => e.doCompany)
                .HasForeignKey(e => e.strFkCompanyCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doCustomer>()
                .Property(e => e.strPkCode)
                .IsUnicode(false);

            modelBuilder.Entity<doCustomer>()
                .Property(e => e.strName)
                .IsUnicode(false);

            modelBuilder.Entity<doCustomer>()
                .Property(e => e.strAddress)
                .IsUnicode(false);

            modelBuilder.Entity<doCustomer>()
                .HasMany(e => e.doOrderDetail)
                .WithRequired(e => e.doCustomer)
                .HasForeignKey(e => e.strFkCustomerCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<doOrderDetail>()
                .Property(e => e.strNumber)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderDetail>()
                .Property(e => e.strFkCompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderDetail>()
                .Property(e => e.strFkCustomerCode)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderDetail>()
                .Property(e => e.decCostPerUnit)
                .HasPrecision(8, 3);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.dtmLoadDate)
                .IsFixedLength();

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strCompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strCompanyCodeFromData)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strOrderNumber)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strCustomerCode)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strCustomerName)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strCustomerAddress)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.strItemDescription)
                .IsUnicode(false);

            modelBuilder.Entity<doOrderFromData>()
                .Property(e => e.decCostPerUnit)
                .HasPrecision(8, 3);

            modelBuilder.Entity<doProduct>()
                .Property(e => e.decCostPerUnit)
                .HasPrecision(8, 3);

            modelBuilder.Entity<doProduct>()
                .HasMany(e => e.doOrderDetail)
                .WithRequired(e => e.doProduct)
                .HasForeignKey(e => e.intFkProduct)
                .WillCascadeOnDelete(false);
        }
    }
}
