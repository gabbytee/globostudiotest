namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderFromData")]
    public partial class doOrderFromData
    {
        [Key]
        public int intId { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] dtmLoadDate { get; set; }

        [Required]
        [StringLength(3)]
        public string strCompanyCode { get; set; }

        [Required]
        [StringLength(2)]
        public string strCompanyCodeFromData { get; set; }

        [Required]
        [StringLength(5)]
        public string strOrderNumber { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime datDateSold { get; set; }

        [Required]
        [StringLength(3)]
        public string strCustomerCode { get; set; }

        [Required]
        [StringLength(100)]
        public string strCustomerName { get; set; }

        [Required]
        [StringLength(100)]
        public string strCustomerAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string strItemDescription { get; set; }

        [NotMapped]
        public int intProductCode
        {
            get
            {
                return Int32.Parse(strItemDescription.Substring(strItemDescription.IndexOf(":") + 1, strItemDescription.Length - strItemDescription.IndexOf(":") - 1));
            }
        }

        [Required]
        public int intQuantity { get; set; }

        [Required]
        public decimal decCostPerUnit { get; set; }
    }
}
