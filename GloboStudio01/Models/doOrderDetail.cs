namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderDetail")]
    public partial class doOrderDetail
    {
        [Key]
        public int intId { get; set; }

        [Required]
        [StringLength(5)]
        public string strNumber { get; set; }

        [Required]
        [StringLength(3)]
        public string strFkCompanyCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime datDateSold { get; set; }

        [Required]
        [StringLength(3)]
        public string strFkCustomerCode { get; set; }

        public int intFkProduct { get; set; }

        public int intQuantity { get; set; }

        public decimal decCostPerUnit { get; set; }

        public virtual doCompany doCompany { get; set; }

        public virtual doCustomer doCustomer { get; set; }

        public virtual doProduct doProduct { get; set; }
    }
}
