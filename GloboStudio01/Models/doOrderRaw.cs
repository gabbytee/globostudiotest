namespace GloboStudio01.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class doOrderRaw
    {
        public string OrderNumber { get; set; }

        public string CompanyCode { get; set; }

        public DateTime DateSold { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAddress { get; set; }

        public string ItemDescription { get; set; }

        public int Quantity { get; set; }

        public decimal CostPerUnit { get; set; }
    }
}
